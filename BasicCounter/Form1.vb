﻿Imports libClasse


Public Class Form1
    Private Sub btnPlus_Click(sender As Object, e As EventArgs) Handles btnPlus.Click
        lblCompteur.Text = class1.incrementation(lblCompteur.Text)
    End Sub

    Private Sub btnMoins_Click(sender As Object, e As EventArgs) Handles btnMoins.Click
        lblCompteur.Text = class1.decrementation(lblCompteur.Text)
    End Sub

    Private Sub btnRaz_Click(sender As Object, e As EventArgs) Handles btnRaz.Click
        lblCompteur.Text = class1.raz(lblCompteur.Text)
    End Sub
End Class
