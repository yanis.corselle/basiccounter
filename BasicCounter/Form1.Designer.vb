﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblTotal = New System.Windows.Forms.Label()
        Me.btnMoins = New System.Windows.Forms.Button()
        Me.btnPlus = New System.Windows.Forms.Button()
        Me.btnRaz = New System.Windows.Forms.Button()
        Me.lblCompteur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LblTotal
        '
        Me.LblTotal.AutoSize = True
        Me.LblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotal.Location = New System.Drawing.Point(181, 40)
        Me.LblTotal.Margin = New System.Windows.Forms.Padding(0)
        Me.LblTotal.Name = "LblTotal"
        Me.LblTotal.Size = New System.Drawing.Size(60, 25)
        Me.LblTotal.TabIndex = 0
        Me.LblTotal.Text = "Total"
        Me.LblTotal.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnMoins
        '
        Me.btnMoins.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoins.Location = New System.Drawing.Point(76, 120)
        Me.btnMoins.Name = "btnMoins"
        Me.btnMoins.Size = New System.Drawing.Size(75, 39)
        Me.btnMoins.TabIndex = 1
        Me.btnMoins.Text = "-"
        Me.btnMoins.UseVisualStyleBackColor = True
        '
        'btnPlus
        '
        Me.btnPlus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPlus.Location = New System.Drawing.Point(266, 121)
        Me.btnPlus.Name = "btnPlus"
        Me.btnPlus.Size = New System.Drawing.Size(75, 39)
        Me.btnPlus.TabIndex = 2
        Me.btnPlus.Text = "+"
        Me.btnPlus.UseVisualStyleBackColor = True
        '
        'btnRaz
        '
        Me.btnRaz.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRaz.Location = New System.Drawing.Point(123, 210)
        Me.btnRaz.Name = "btnRaz"
        Me.btnRaz.Size = New System.Drawing.Size(171, 34)
        Me.btnRaz.TabIndex = 3
        Me.btnRaz.Text = "Remise à zéro"
        Me.btnRaz.UseVisualStyleBackColor = True
        '
        'lblCompteur
        '
        Me.lblCompteur.AutoSize = True
        Me.lblCompteur.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompteur.Location = New System.Drawing.Point(198, 121)
        Me.lblCompteur.Name = "lblCompteur"
        Me.lblCompteur.Size = New System.Drawing.Size(31, 33)
        Me.lblCompteur.TabIndex = 4
        Me.lblCompteur.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(411, 308)
        Me.Controls.Add(Me.lblCompteur)
        Me.Controls.Add(Me.btnRaz)
        Me.Controls.Add(Me.btnPlus)
        Me.Controls.Add(Me.btnMoins)
        Me.Controls.Add(Me.LblTotal)
        Me.Name = "Form1"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblTotal As Label
    Friend WithEvents btnMoins As Button
    Friend WithEvents btnPlus As Button
    Friend WithEvents btnRaz As Button
    Friend WithEvents lblCompteur As Label
End Class
